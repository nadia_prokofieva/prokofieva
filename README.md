1 block:

Logo: BINO
1 part B: font OpenSans Bold, size 36px, colors 4a4c55, f1ebe7, left part of the page.
2 part INO: font OpenSans Light, size 36px, colors 4a4c55, f1ebe7, left part of the page.

Menu: HOME  
Details: OpenSans Bold, size 14px, color e74c3c, main part of the page.

Menu: ABOUT US PORTFOLIO PRICING TEAM BLOG CONTACT 
Details: OpenSans Bold, size 14px, color e74c3c.

Text: Our Clients Are Our First Priority 
Details: font OpenSans Regular, size 24px, color f4f5f9, main part of the page.

Text: WELCOME TO BINO 
Details: font OpenSans Bold, size 72px, color f4f5f9, main part of the page.

Text: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
Details: font OpenSans Regular, size 24px, color f4f5f9, main part of the page.

Botton 1: GET STARTED NOW
Details: OpenSans Bold, size 14px, color ffffff.

Botton 2: LEARN MORE
Details: OpenSans Bold, size 14px, color ffffff.


2 block:

Text: SLEEK DESIGN 
Details: font OpenSans Semibold, size 18px, color e74c3c.

Text: CLEAN CODE/ CREATIVE IDEAS/ FREE SUPPORT
Details: font OpenSans Semibold, size 18px, color 6a6a6a.

Text: Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been the industry.
Details: font SourceSansPro Light, size 12px, color 999999.

Text: OUR HISTORY
Details: font SourceSansPro Bold, size 36px, 6a6a6a

Text: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip sum has been the industry's standard dummy text ever since the 1500s, when an unk- nown printer took a galley of type and scrambled it to make a type specimen book. 
It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
Details: font SourceSansPro Bold, size 14px, 999999

Botton: BROWSE OUR WORK
Details: OpenSans Bold, size 14px, color ffffff.


3 block:
Text: OUR SERVICES
Details: font SourceSansPro Bold, size 36px, color f4f5f9, main part of the page.

Text: WEB DESIGN
Details: font OpenSans Semibold, size 18px, color e74c3c.

Text: Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ip sum has been the industry's standard dummy text ever.
Details: font SourceSansPro Regular, size 14px, color 999999.

Text: PRINT DESIGN 
Details: font OpenSans Semibold, size 18px, color e74c3c.

Text: PHOTOGRAPHY
Details: font OpenSans Semibold, size 18px, color e74c3c.


4 block:
Text: RECENT WORKS
Details: font SourceSansPro Bold, size 36px, color 6a6a6a, main part of the page.

Text: It has survived not only five centuries, but also the leap scrambled it to make a type.
Details: font OpenSans, size 14px, color 999999, main part of the page.

Text: ALL
Details: font OpenSans Bold, size 14px, color e74c3c, main part of the page.

Text: PRINT DESIGN/ ANIMATION/ ART/ WEB DESIGN/ PHOTOGRAPHY/ VIDEO
Details: font OpenSans, size 14px, color 999999, main part of the page.

Text: T-SHIRT DESIGN
Details: font OpenSans Bold, size 24px, color ffffff, inside picture.

Text: art / t-shirt
Details: font OpenSans, size 14px, color ffffff, inside picture.


5 block:

Text: CASE STUDY
Details: font SourceSansPro Bold, size 36px, color 6a6a6a, main part of the page.

Text: A brief story about how this process works, keep an eye till the end.
Details: font OpenSans, size 14px, color 999999, main part of the page.

Text: ACCUMULATE CREATIVE IDEAS
Details: font OpenSans Semibold, size 18px, color 6a6a6a.

Text: Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been the industry. Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been the industry Printing and typelorem Ipsum has been the setting let.
Details: font SourceSansPro Light, size 14px, color 999999.

Botton: READ MORE
Details: font OpenSans Semibold, size 12px, color ffffff.

Numbers: 3891/ 281K/ 618/ 178/ 285
Details: font SourceSansPro Bold, size 30px, color f4f5f9.

Text: User Favourites
Details: font SourceSansPro Regular, size 18px, color f4f5f9.


6 block:

Text: OUR PRICING
Details: font SourceSansPro Bold, size 36px, color 6a6a6a, main part of the page.

Text: A 30 days free trial for all. A brief story about how this process works, keep an eye till the end.
Details: font OpenSans, size 14px, color 999999, main part of the page.

Inside pricing plan pictures: 
Text: STARTER/ PREMIUM/ BUSINESS
Details: font OpenSans Semibold, size 24px, color ffffff.

Text: $19/ $39 / $99
Details: font SourceSansPro Bold, size 39,51px, color for $39 e74c3c, color 999999 for $19/ $99.

Text: per month
Details: font SourceSansPro Regular, size 13,17px, color 999999.

Text: Competition Analysis Methods
All Ranked URLs
International Support System
Social Media Tracking
Details: font OpenSans Semibold, size 14px, color 999999.

Bottons: CHOOSE PLAN
Details: font OpenSans Semibold, size 12px, color ffffff.


7 block:

Text: OUR TEAM
Details: font SourceSansPro Bold, size 36px, color ffffff, main part of the page.

Text: Meet the craziest team. Share your thoughts with them.
Details: font OpenSans, size 14px, color f4f5f9, main part of the page.

Text: Kazi Erfan
Details: font OpenSans Semibold, size 24px, color e74c3c.


8 block:
Text: Great Integrations with Others
Details: font SourceSansPro Bold, size 36px, color 6a6a6a, main part of the page.

Text: Suspendisse sed eros mollis, tincidunt felis eget, interdum erat. 
Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec. 
Details: font OpenSans, size 14px, color 999999, main part of the page.


9 block:

Text: OUR BLOG
Details: font SourceSansPro Bold, size 36px, color 6a6a6a, main part of the page.

Text: Suspendisse sed eros mollis, tincidunt felis eget, interdum eratullam sit amet odio.
Details: font OpenSans, size 14px, color 999999, main part of the page.

Text: art / t-shirt
Details: font OpenSans, size 14px, color ffffff, main part of the page.

Text: T-SHIRT DESIGN
Details: font OpenSans Semibold, size 24px, color ffffff.

Text: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indu Stry's standard dummy text ever since the 1500s, an unknown printer took a galley of type a scrambled it to make a type specimen book.
Details: OpenSans, size 16px, color ffffff.


10 block:

Text: kEEP IN TOUCH
Details: font SourceSansPro Bold, size 36px, color 6a6a6a, main part of the page.

Text: Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec. 
Details: font OpenSans, size 14px, color 999999, main part of the page.

Text: OUR ADDRESS/ CALL US/ EMAIL US
Details: font SourceSansPro Bold, size 24px, color e74e3e.

Text: House #13, Streat road, Sydney 
2310 Australia/
+ 880 168 109 1425
+ 0216809142/
contactus@email.com
Details: font OpenSans, size 18px, color 999999, main part of the page.

Text: Name/ Email/ Subject/ Message
Details: font OpenSans Semibold, size 18px, color 999999.

Botton: BROWSE OUR WORK
Details: OpenSans Bold, size 14px, color ffffff.

11 block:

Text: Let's Get Started Now. It's FREE!
Details: OpenSans/ OpenSans Bold, size 36px, color ffffff.

Text: 30 day free trial. Free plan allows up to 2 projects. Pay if you need more. Cancel anytime. No catches.
Details: OpenSans/ OpenSans Bold, size 18px, color ffffff.

Botton: start free trial
Details: OpenSans Bold, size 14px, color ffffff.